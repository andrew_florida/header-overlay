class Header {
    constructor() {
        
    }
    sticky() {/**Header style sticky */
        window.onscroll = function() { 
            stickyHeader()
        };

        let header = document.querySelector("header");
        let sticky = header.offsetTop;

        function stickyHeader() {
        if (window.pageYOffset > sticky) {
            header.classList.add("a-sticky");
        } else {
            header.classList.remove("a-sticky");
        }
        }
    }


    lazy(){/**Hide navbar on scroll bottom */
        window.onscroll = function() { 
            stickyHeader()
        };

        let header = document.querySelector("header");
        header.classList.add('sticky');
        let show = header.querySelector('.header__holder');
        let prevScrollpos = window.pageYOffset;
        window.onscroll = function () {
            let currentScrollPos = window.pageYOffset;
            if (prevScrollpos > currentScrollPos && prevScrollpos > 100) {
              header.classList.add("a-sticky");
              show.classList.remove("header--hidden");
            } else if (prevScrollpos < 100) {
              header.classList.remove("a-sticky");
              show.classList.remove("header--hidden");
            } else {
              header.classList.remove("a-sticky");
              show.classList.add("header--hidden");
            }
            prevScrollpos = currentScrollPos;
        }
    }

    login(){
        let loginShow = document.getElementById("header__login-button");
        loginShow.addEventListener('click', function () {
            document.querySelector(".header__login-register").classList.toggle('show');
        }); 
    }

    navToggle(){
        let openMenuButton = document.querySelector('.header__trigger');
        let closeMenuButton = document.querySelector('.fullscreen__nav-close');
        let openMenu = document.querySelector('.fullscreen__nav');

        openMenuButton.addEventListener('click', function () {
            console.log(5)
            openMenu.classList.add('opened');
        });

        closeMenuButton.addEventListener('click', function () {
            openMenu.classList.remove('opened');
        });
    }

    dropdown() {
        let dropdown = document.getElementsByClassName("menu__sub-level-arrow");
        let i;

        for (i = 0; i < dropdown.length; i++) {
            dropdown[i].addEventListener("click", function () {
                let thisSubMenu = this;
                let dropdownContent = thisSubMenu.nextElementSibling;
                if (dropdownContent.style.display === "block") {
                    dropdownContent.style.display = "none";
                } else {
                    dropdownContent.style.display = "block";
                }
            });
        }
    }

    mobileToolbarToggle(){
        let toggleBar = document.querySelector(".header__toolbar-resposnive-icon");
        let toolbarMobile = document.querySelector(".header__toolbar");

        toggleBar.addEventListener('click', function(){
            toolbarMobile.style.display === "block" ? toolbarMobile.style.display = "none" : toolbarMobile.style.display = "block";
        });
    }
}

let myHeader = new Header();
    myHeader.login();
    myHeader.navToggle();
    myHeader.dropdown();
    myHeader.lazy();
    myHeader.mobileToolbarToggle();
